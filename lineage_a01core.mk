#
# Copyright (C) 2022 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from a01core device
$(call inherit-product, device/samsung/a01core/device.mk)

PRODUCT_DEVICE := a01core
PRODUCT_NAME := lineage_a01core
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-M013F
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="a01coredd-user 10 QP1A.190711.020 M013FDDU6AVC5 release-keys"

BUILD_FINGERPRINT := samsung/a01coredd/a01core:10/QP1A.190711.020/M013FDDU6AVC5:user/release-keys
